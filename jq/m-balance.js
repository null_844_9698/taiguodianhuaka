$(document).ready(function () {
    $('.main-left ul li').click(function () {
        var $this = $(this);
        if($this.index() === 0){
            window.location.href = 'm-order.html';
        } else if($this.index() === 1) {
            window.location.href = 'm-address.html';
        } else if($this.index() === 2) {
            window.location.href = 'm-safe.html';
        } else if($this.index() === 3) {
            window.location.href = 'm-balance.html';
        } else if($this.index() === 4) {
            window.location.href = 'm-after.html';
        } else if($this.index() === 5) {
            window.location.href = 'm-message.html';
        }
    });
    $('.cont-sel span').click(function () {
        $('.sel-state').toggle(0);
    });
    $('.sel-state li').click(function () {
        var $this = $(this);
        var thisText = $this.text();
        $('.cont-sel span').text(thisText);
        $('.sel-state').toggle(0);
    });
    $('.main-left ul li').eq(3).click(function () {
        $('.charge').hide();
        $('.cash').hide();
        $('.index').show();
    });
    $('.cont-2-charge').click(function () {
        $('.index').hide();
        $('.cash').hide();
        $('.charge').show();
    });
    $('.cont-2-cash').click(function () {
        $('.index').hide();
        $('.charge').hide();
        $('.cash').show();
    });
    $('.bank-card ul li').hover(function () {
        var $this = $(this);
        $this.find('p').show();
    },function () {
        var $this = $(this);
        $this.find('p').hide();
    });
    $('.bank-card ul li p').hover(function () {
        var $this = $(this);
        $this.css('background-color','rgba(43,43,43,1)');
    },function () {
        var $this = $(this);
        $this.css('background-color','rgba(43,43,43,0.6)');
    });
    $('.type-main span').click(function () {
        $(this).siblings().removeClass('type-checked');
        $(this).addClass('type-checked');
    });



    /*弹出层*/
    var $mask = $('.mask');
    var $pop = $('.pop');

    $('.add-card').click(function () {
        $mask.show();
        $pop.show();
    });
    $('.close').click(function () {
        hideAll();
    });
    /*TODO 点击保存后页面对修改做响应*/
    $('.save').click(function () {
        hideAll();
    });
    $('.cancel').click(function () {
        hideAll();
    });
    $mask.click(function () {
        hideAll();
    });
    function hideAll() {
        $mask.hide();
        $pop.hide();
    }
});