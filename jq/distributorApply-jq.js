/* add by liushang 2017/7/10*/

//文本框提示文字
function textFocus(el) {
    if (el.defaultValue == el.value) { el.value = ''; el.style.color = '#333'; }
}
function textBlur(el) {
    if (el.value == '') { el.value = el.defaultValue; el.style.color = '#999'; }
}

$(document).ready(function(){
    (function validatePhone() {
        $(".section2-content-basic .phone").blur(function () {
            reg = /^1[3|4|5|8][0-9]\d{4,8}$/i;//验证手机正则(输入前7位至11位)

            if ($(this).val() == "" || $(this).val() == "请输入您的账号") {
                $(this).addClass("errorC");
                $(this).next().html("账号不能为空！");
            }
            else if ($(".section2-content-basic .phone").val().length < 11) {
                $(this).addClass("errorC");
                $(this).next().html("账号长度有误！");
            }
            else if (!reg.test($(".section2-content-basic .phone").val())) {
                $(this).addClass("errorC");
                $(this).next().html("账号不存在!");
            }
            else {
                $(this).addClass("checkedN");
                $(this).removeClass("errorC");
                $(this).next().empty();
            }
        });
    })();
    $('input[name=uploadfile]').on('change', function () {
        var $this = $(this);
        lrz(this.files[0], {width: 220}).then(function (rst) {
            console.log(rst.base64);
            var attrSrc = '<img data-src=""" src="' + rst.base64 + '" alt="" />';
            $this.parent().find('.img-list').append(attrSrc);
            $this.parent().find('.img-list').show();
            $this.parent().find('a').hide();
            /*$.ajax({
             url: '',
             type: 'post',
             data: {img: rst.base64},
             dataType: 'json',
             timeout: 200000,
             success: function (response) {
             if (response.ecd == '0') {
             alert('成功');
             return true;
             } else {
             return alert(response.msg);
             }
             },
             error: function (jqXHR, textStatus, errorThrown) {

             if (textStatus == 'timeout') {
             alert('请求超时');
             return false;
             }
             alert(jqXHR.responseText);
             }
             });*/
        }).catch(function (err) {

        }).always(function () {

        });
    });
    $('.delete').hover(function(){
        $(this).css("opacity", "1");
    },function () {
        $(this).css("opacity", "0.8");
    }).click(function () {
        $('input[name=uploadfile]').val('');
        $(this).parent().find('img').remove();
        $(this).parent().parent().find('a').show();
        $(this).parent().parent().find('.img-list').hide();
    });
    $('.img-list').hover(function(){
        $(this).find("p").show();
    },function () {
        $(this).find("p").hide();
    });
});





