$(document).ready(function () {
    $('.orderstate span').click(function () {
        $('.sel-state').toggle(0);
    });
    $('.sel-state li').click(function () {
        var $this = $(this);
        var thisText = $this.text();
        $('.orderstate span').text(thisText);
        $('.sel-state').toggle(0);
    });
    $('.main-left ul li').click(function () {
        var $this = $(this);
        if($this.index() === 0){
            window.location.href = 'm-order.html';
        } else if($this.index() === 1) {
            window.location.href = 'm-address.html';
        } else if($this.index() === 2) {
            window.location.href = 'm-safe.html';
        } else if($this.index() === 3) {
            window.location.href = 'm-balance.html';
        } else if($this.index() === 4) {
            window.location.href = 'm-after.html';
        } else if($this.index() === 5) {
            window.location.href = 'm-message.html';
        }
    });
})