/*add by liushang 2017/7/8*/

$(document).ready(function(){
    var acceptBtn = $('#acceptBtn');
    var submitBtn = $('#submitBtn');
    submitBtn.click(function(e){
        console.log(acceptBtn.prop('checked'));
        if(!acceptBtn.prop('checked')){
            alert("请勾选同意");
            e.preventDefault();
        }
    });
    $('.main-warp0').show();
    $('.main-head span').click(function () {
        var temp = $(this).index();
        var showWT = ".main-part" + temp;
        console.log(showWT)
        hideAll();
        switch(temp){
            case 0:
                $('.main-warp0').show();
                break;
            case 1:
                $('.main-warp1').show();
                break;
            case 2:
                $('.main-warp2').show();
                break;
            case 3:
                $('.main-warp3').show();
                break;
        }
    });
    function hideAll() {
        $('.main-warp0').hide();
        $('.main-warp1').hide();
        $('.main-warp2').hide();
        $('.main-warp3').hide();
    }
});


