$(document).ready(function () {
    $('.main-left ul li').click(function () {
        var $this = $(this);
        if($this.index() === 0){
            window.location.href = 'm-order.html';
        } else if($this.index() === 1) {
            window.location.href = 'm-address.html';
        } else if($this.index() === 2) {
            window.location.href = 'm-safe.html';
        } else if($this.index() === 3) {
            window.location.href = 'm-balance.html';
        } else if($this.index() === 4) {
            window.location.href = 'm-after.html';
        } else if($this.index() === 5) {
            window.location.href = 'm-message.html';
        }
    });
});