$(document).ready(function () {
    $('input[name=uploadfile]').on('change', function () {
        var $this = $(this);
        lrz(this.files[0], {width: 220}).then(function (rst) {
            console.log(rst.base64);
            var attrSrc = '<img data-src=""" src="' + rst.base64 + '" alt="" />';
            $this.parent().find('.img-list').append(attrSrc);
            $this.parent().find('.img-list').show();
            $this.parent().find('a').hide();
            /*$.ajax({
             url: '',
             type: 'post',
             data: {img: rst.base64},
             dataType: 'json',
             timeout: 200000,
             success: function (response) {
             if (response.ecd == '0') {
             alert('成功');
             return true;
             } else {
             return alert(response.msg);
             }
             },
             error: function (jqXHR, textStatus, errorThrown) {

             if (textStatus == 'timeout') {
             alert('请求超时');
             return false;
             }
             alert(jqXHR.responseText);
             }
             });*/
        }).catch(function (err) {

        }).always(function () {

        });
    });
    $('.delete').hover(function(){
        $(this).css("opacity", "1");
    },function () {
        $(this).css("opacity", "0.8");
    }).click(function () {
        $('input[name=uploadfile]').val('');
        $(this).parent().find('img').remove();
        $(this).parent().parent().find('a').show();
        $(this).parent().parent().find('.img-list').hide();
    });
    $('.img-list').hover(function(){
        $(this).find("p").show();
    },function () {
        $(this).find("p").hide();
    });
});