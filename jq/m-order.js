$(document).ready(function () {
    // TODO 检测商品图片宽高，使在不改变宽高比的情况下自适应宽高
    /*$('.item-main .item-pic').find('img').on('load',function () {
        var $this = $(this);
        var picWidth =  $this.width();
        var picHeight = $this.height();
        console.log('高：'+ picHeight + ';' + '宽：' + picWidth );
        if(picWidth >= picHeight){
            $this.css('height','100%');
        } else if(picHeight > picWidth){
            $this.css('width','100%');
        }
    });*/

    $('.main-head ul li').click(function () {
        var $this = $(this);
        var allLi = $('.cont-main li');
        $this.addClass('active');
        $this.addClass('action');
        $this.siblings().removeClass('active');
        $this.siblings().removeClass('action');
        allLi.hide();
        if($this.index() === 0){
            allLi.show();
        } else if($this.index() === 1){
            $('.state0').show();
        } else if($this.index() === 2){
            $('.state1').show();
        } else if($this.index() === 3){
            $('.state2').show();
        } else if($this.index() === 4){
            $('.state3').show();
        }
    });

    $('.main-left ul li').click(function () {
        var $this = $(this);
        if($this.index() === 0){
            window.location.href = 'm-order.html';
        } else if($this.index() === 1) {
            window.location.href = 'm-address.html';
        } else if($this.index() === 2) {
            window.location.href = 'm-safe.html';
        } else if($this.index() === 3) {
            window.location.href = 'm-balance.html';
        } else if($this.index() === 4) {
            window.location.href = 'm-after.html';
        } else if($this.index() === 5) {
            window.location.href = 'm-message.html';
        }
    });

    $('.go-pay').click(function () {
        //window.location.href = 'm-cart.html';
    });

    /*弹出层*/
    var mask = $('.mask');
    var popDele = $('.pop-dele');

    mask.click(function () {
        mask.hide();
        popDele.hide();
    });

    $('.dele').click(function () {
        mask.show();
        popDele.show();
    });

    $('.dele-confirm').click(function () {
        //TODO
        mask.hide();
        popDele.hide();
    });

    $('.dele-cancel').click(function () {
        mask.hide();
        popDele.hide();
    });

});