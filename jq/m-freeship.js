$(document).ready(function () {
    var mask = $('.mask');
    var popSel = $('.pop-sel');

    mask.click(function () {
        mask.fadeOut('fast');
        popSel.fadeOut('fast');
    });

    $('.close').click(function () {
        mask.fadeOut('fast');
        popSel.fadeOut('fast');
    });

    var goodsIndex; //获得所点击商品的索引 TODO 这里根据索引值判断所点击的是哪一个商品，需要更改
    $('.section4-pro-lis-txts button').on('click', function (e) {
        e.preventDefault();
        var $this = $(this);
        if ($this.parent().hasClass('sec4-cancel-btn')) { //如果按钮是取消，即该商品已在购物车中
            $this.parent().removeClass('sec4-cancel-btn').addClass('sec4-select-btn').find('button').text('加入购物车'); //点击取消按钮可从购物车中移除
        } else { //商品未被选在购物车中
            //弹出层中选择套餐
            mask.fadeIn('fast');
            popSel.fadeIn('fast');
            goodsIndex = $this.parents('.section4-pro-ul > a').index(); //获得所点击商品的索引 TODO 这里根据索引值判断所点击的是哪一个商品，需要更改
        }
    });


    //点击弹出层中的确认按钮
    $('.pop-foot button').click(function () {
        var thatBtn = $('.section4-pro-ul > a').eq(goodsIndex).find('button'); //根据索引值得到商品的状态按钮
        //当选项中都有被选中了
        if($('.main-type ul li').hasClass('menu-action') && $('.main-menu ul li').hasClass('menu-action')){
            thatBtn.parent().removeClass('sec4-select-btn').addClass('sec4-cancel-btn').find('button').text('取消');
            mask.fadeOut('fast');
            popSel.fadeOut('fast');
        }
    }).hover(function () { //当选项中都有被选中了，添加hover效果
        var $this = $(this);
        if($this.hasClass('foot-btn-active')){
            $this.css('background-color','#f46425');
        }
    },function () {
        var $this = $(this);
        if($this.hasClass('foot-btn-active')){
            $this.css('background-color','#fe8550');
        }
    });

    //点击选择取件方式
    $('.main-type ul li').click(function () {
        var $this = $(this);
        $this.addClass('menu-action');
        $this.siblings().removeClass('menu-action');
        //判断是否启用确认按钮
        if($this.hasClass('menu-action') && $('.main-menu ul li').hasClass('menu-action')){
            $('.pop-foot > button').addClass('foot-btn-active');
        }
    });

    //点击选择套餐类型
    $('.main-menu ul li').click(function () {
        var $this = $(this);
        $this.addClass('menu-action');
        $this.siblings().removeClass('menu-action');
        //判断是否启用确认按钮
        if($this.hasClass('menu-action') && $('.main-type ul li').hasClass('menu-action')){
            $('.pop-foot > button').addClass('foot-btn-active');
        }
    });

    //点击加减数量
    var num = $('.num-input').attr('value');
    $('.num-sub').click(function () {
        num--;
        if(num <= 1){
            $(this).css('background-color', '#fafafa');
            num = 1;
        }
        $('.num-input').attr('value', num);
    });

    $('.num-add').click(function () {
        num++;
        $('.num-sub').css('background-color', '#f6f6f6');
        $('.num-input').attr('value', num);
    });
});

