$(document).ready(function () {
    $('.main-left ul li').click(function () {
        var $this = $(this);
        if ($this.index() === 0) {
            window.location.href = 'm-order.html';
        } else if ($this.index() === 1) {
            window.location.href = 'm-address.html';
        } else if ($this.index() === 2) {
            window.location.href = 'm-safe.html';
        } else if ($this.index() === 3) {
            window.location.href = 'm-balance.html';
        } else if ($this.index() === 4) {
            window.location.href = 'm-after.html';
        } else if ($this.index() === 5) {
            window.location.href = 'm-message.html';
        }
    });
    /*点击弹出层中的选中默认*/
    $('.set-img').click(function () {
        var $this = $(this);
        if ($this.hasClass('set-default-2')) {
            $this.removeClass('set-default-2');
        } else {
            $this.addClass('set-default-2');
        }
    });
    /*弹出层控制*/
    var $mask = $('.mask');
    var $pop = $('.pop');
    var $popDele = $('.pop-dele');
    var thisIndex; //标志当前点击的是哪一个li，用于指定删除时
    /*点击新增地址*/
    $('.add-address').click(function () {
        $mask.show();
        $pop.show();
        $('.area-select').distpicker();
        /*清空所有输入框，以防出现bug*/
        $('.pop h3').text('新增地址');
        $('.pop-name input').val('');
        $('.pop-phone input').val('');
        $('.pop-detail input').val('');
        $('.set-img').addClass('set-default-2');

    });
    /*点击编辑收货地址*/
    $('.editing').click(function () {
        var $this = $(this);
        var selected = $this.parent().find('span').hasClass('default');
        var name = $this.parent().parent().parent().find('.item-name').text();
        var phone = $this.parent().parent().parent().find('.item-phone').text();
        var province = $this.parent().parent().parent().find('.province').text();
        var city = $this.parent().parent().parent().find('.city').text();
        var area = $this.parent().parent().parent().find('.area').text();
        var address = $this.parent().parent().parent().find('.address').text();
        $mask.show();
        $pop.show();
        $('.pop h3').text('编辑地址');
        $('.pop-name input').val(name);
        $('.pop-phone input').val(phone);
        $('.area-select').distpicker({
            province: province,
            city: city,
            district: area
        });
        $('.pop-detail input').val(address);
        if (selected) {
            $('.set-img').addClass('set-default-2');
        } else {
            $('.set-img').removeClass('set-default-2');
        }
        //TODO 地址选择默认选中
    });
    $('.delete').click(function () {
        thisIndex = $(this).parents('.con-item').index(); //标志当前点击的是哪一个li，用于指定删除时
        $mask.show();
        $popDele.show();
    });
    //确定删除时
    $('.dele-confirm').click(function () {
        $('.con-item').eq(thisIndex).remove();
        hideAll();
        if(!$('.con-main').hasClass('default')){
            $('.con-item').eq(0).find('.item-edit .set-default').addClass('default').removeClass('set-default').text('默认');
        }
        //TODO AJAX交互
    });

    $('.dele-cancel').click(function () {
        hideAll();
    });
    $('.close').click(function () {
        hideAll();
    });
    $('.save').click(function () {
        //TODO
        var foo = $('.con-item').eq(0).clone();
        foo.prependTo('.con-main');
        hideAll();
    });
    $('.cancel').click(function () {
        hideAll();
    });
    $mask.click(function () {
        hideAll();
    });
    function hideAll() {
        $mask.hide();
        $pop.hide();
        $popDele.hide();
    }
});
/*点击改变收货地址列表中选中默认*/
$('.item-edit span').on('click',function () {
    var $this = $(this);
    var $that = $('.default');
    if($this.hasClass('set-default')){
        $this.addClass('default').removeClass('set-default').text('默认').parents('.con-item').prependTo('.con-main');
        $that.addClass('set-default').removeClass('default').text('设为默认');
    }
});