$(document).ready(function () {
    /*选择支付方式*/
    $('.part2-left i').click(function () {
        var $this = $(this);
        $this.addClass('selected');
        $this.parent().siblings().find('i').removeClass('selected');
    });
    /*弹出层*/
    var $mask = $('.mask');
    var $pop = $('.pop');
    $('.close').click(function () {
        hideAll();
    });
    $('.cancel').click(function () {
        hideAll();
    });
    $mask.click(function () {
        hideAll();
    });
    function hideAll() {
        $mask.fadeOut('fast');
        $pop.fadeOut('fast');
    }

    /*点击保存*/
    $('.save').click(function () {
        //TODO
        // console.log($('#s_province option:selected').text() + $('#s_city option:selected').text() + $('#s_county option:selected').text());
        var $this = $(this);
        if ($this.hasClass('save-active')) {
            var newAddress = JSON.parse(localStorage.getItem('NewAddress'));
            $('.item-name').text(newAddress.name);
            $('.item-phone').text(newAddress.phone);
            $('.province').text(newAddress.province);
            $('.city').text(newAddress.city);
            $('.area').text(newAddress.county);
            $('.address').text(newAddress.detail);
            hideAll();
        }
    });

    /*点击修改地址*/
    $('.editing').click(function () {
        var $this = $(this);
        var selected = $this.parent().parent().find('i').hasClass('default-add-icon');
        var name = $this.parent().parent().find('.item-name').text();
        var phone = $this.parent().parent().find('.item-phone').text();
        var province = $this.parent().parent().find('.province').text();
        var city = $this.parent().parent().find('.city').text();
        var area = $this.parent().parent().find('.area').text();
        var address = $this.parent().parent().find('.address').text();
        $mask.fadeIn('fast');
        $pop.fadeIn('fast');
        $('.pop h3').text('编辑地址');
        $('.pop-name input').val(name);
        $('.pop-phone input').val(phone);
        $('.area-select').distpicker({
            province: province,
            city: city,
            district: area
        });
        $('.pop-detail input').val(address);
        if (selected) {
            $('.set-img').addClass('set-default-2');
        } else {
            $('.set-img').removeClass('set-default-2');
        }

        //TODO 地址选择默认选中
    });
    /*点击新增地址*/
    $('.add-address').click(function () {
        $mask.fadeIn('fast');
        $pop.fadeIn('fast');
        /*清空所有输入框，以防出现bug*/
        $('.pop h3').text('新增地址');
        $('.pop-name input').val('');
        $('.pop-phone input').val('');
        $('.pop-detail input').val('');
        $('.area-select').distpicker('reset', true);
        $('.set-img').addClass('set-default-2');
    });
    /*/!*点击弹出层中的选中默认*!/
    $('.set-img').click(function () {
        var $this = $(this);
        if ($this.hasClass('set-default-2')) {
            $this.removeClass('set-default-2');
        } else {
            $this.addClass('set-default-2');
        }
    });*/
});

$('.pop-name input').on('input', function () {
    saveActive();
});

$('.pop-phone input').on('input', function () {
    saveActive();
});

$('.pop-detail input').on('input', function () {
    saveActive();
});

$('#s_province').on('change', function () {
    saveActive();
});

$('#s_city').on('change', function () {
    saveActive();
});

$('#s_county').on('change', function () {
    saveActive();
});

function saveActive() {
    var name = $('.pop-name input').val(),
        phone = $('.pop-phone input').val(),
        province = $('#s_province option:selected').attr('data-text'),
        city = $('#s_city option:selected').attr('data-text'),
        county = $('#s_county option:selected').attr('data-text'),
        detail = $('.pop-detail input').val();
    var newAddress = {
        name: name,
        phone: phone,
        province: province,
        city: city,
        county: county,
        detail: detail
    };
    localStorage.setItem('NewAddress', JSON.stringify(newAddress));
    if (name !== '' && phone !== '' && province !== '' && city !== '' && county !== '' && detail !== '') {
        $('.save').addClass('save-active');
    } else {
        $('.save').removeClass('save-active');
    }
}

