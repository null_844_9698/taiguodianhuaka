// JavaScript Document
$(function () {

    $('.section4-pro-lis').hover(function () {
        $(this).find('.section4-pro-lis-imgs').css('transform', 'scale(1.2)');
    }, function () {
        $(this).find('.section4-pro-lis-imgs').css('transform', 'scale(1)');
    });

    /* 轮播图*/
    !function () {
        //获取图片的下标长度
        $(".img-box").width($(".img-box img").length * 1200);
        //初始化
        var n = 0;

        function run() {
            if (n < $(".img-box img").length - 1) {
                n += 1;
            } else {
                n = 1;
                $(".img-box").css({
                    marginLeft: 0
                });
            }
            $(".img-box").animate({
                    marginLeft: -1200 * n
                },
                500);
            $(".btn li").removeClass("Red");
            if (n == $(".img-box img").length - 1) {
                $(".btn li").eq(0).addClass("Red");
            } else {
                $(".btn li").eq(n).addClass("Red");
            }
        }

        //设置定时器
        var Timer = setInterval(run, 4000);
        $(".btn li").hover(function () {
                clearInterval(Timer);
                //获取图片的位置
                n = $(".btn li").index(this);
                $(".img-box").animate({
                        marginLeft: -1200 * n
                    },
                    500);
                $(".btn li").removeClass("Red").eq(n).addClass("Red");
            },
            function () {
                Timer = setInterval(run, 5000);
            });
    }();

    !
        function () {
            $(".container img .container dd").hide().first().show();
            //$(".container img,.container dd").hide();
            //$(".container img:eq(0),.container dd:eq(0)").show();
            var n = 0;

            function run() {
                if (n < $(".container img").length - 1) {
                    n += 1
                } else {
                    n = 0;
                }
                $(".container img,.container dd").hide();
                $(".container img:eq(" + n + "),.container dd:eq(" + n + ")").show();
            }

            var setTimer = setInterval(run, 5000);
            //鼠标经过控制轮换图的动态
            $(".container").hover(function () {
                    clearInterval(setTimer);
                },
                function () {
                    setTimer = setInterval(run, 5000);
                });

        }();
    /*分类选项*/
    var oTab = document.getElementById("cardType_id");
    var aH3 = oTab.getElementsByTagName("span");
    aH3[0].style.background = "url(images/list/selected.png) no-repeat right";

    var oCard = document.getElementById("card_content");
    var aDiv = oCard.getElementsByClassName("tyep_0");

    for (var i = 0; i < aH3.length; i++) {
        aH3[i].index = i;
        aH3[i].onclick = function () {
            for (var i = 0; i < aH3.length; i++) {
                aH3[i].style.background = "url(images/list/select.png) no-repeat right";
                aDiv[i].style.display = "none";

            }

            this.style.background = "url(images/list/selected.png) no-repeat right";
            aDiv[this.index].style.display = "block";
        }
    }

    /*区域选项卡*/
    var global = document.getElementById("center_global");
    var arr_global = global.getElementsByTagName("span");
    var country = document.getElementById("city_center");
    var arr_country = country.getElementsByClassName("center_country");
    var more_country = country.getElementsByClassName("all_city");
    var up_down_01 = document.getElementById("unfold_01");
    /*第一次默认的选中*/
    arr_global[0].style.background = "#f6f6f6";
    arr_global[0].style.color = "#ff6633";
    var mq_flog_04 = -1;
    up_down_01.onclick = function () {
        if (mq_flog_04 == -1) {
            more_country[0].style.display = "block";
            up_down_01.style.background = "url(images/list/down.png) no-repeat right";
            up_down_01.innerHTML = "收起";
        } else {
            more_country[0].style.display = "none";
            up_down_01.style.background = "url(images/list/up.png) no-repeat right";
            up_down_01.innerHTML = "展开";
        }
        mq_flog_04 = mq_flog_04 * -1;
    }

    /*区域城市选中后改变颜色*/
    $('.center_country span').click(function () {

        $(this).toggleClass('span-active');
        var is_cf = isrepetition($(this).text());
        var is_kong = is_temp();
        if (is_kong) {

        } else {
            $('#all_selected_del').text("清空已选");
        }

        if (is_cf) {
            $('#condition_selected_del').append('<span>' + $(this).text() + '</span>');
            shanchu();
        } else {
            alert("'" + $(this).text() + "'" + '已经被添加过了！');
        }

    });


    /*城市选择选项卡 外加 更多城市的收起和展开*/


    for (var i = 0; i < arr_global.length; i++) {
        arr_global[i].index = i;
        arr_global[i].onmouseover = function () {
            for (var i = 0; i < arr_global.length; i++) {
                arr_global[i].style.background = "none";
                arr_global[i].style.color = "#666666";
                arr_country[i].style.display = "none";
                more_country[i].style.display = "none";
            }
            var mq_index = this.index;
            var mq_flog_01 = -1;
            up_down_01.onclick = function () {
                if (mq_flog_01 == -1) {
                    more_country[mq_index].style.display = "block";
                    up_down_01.style.background = "url(images/list/down.png) no-repeat right";
                    up_down_01.innerHTML = "收起";
                } else {
                    more_country[mq_index].style.display = "none";
                    up_down_01.style.background = "url(images/list/up.png) no-repeat right";
                    up_down_01.innerHTML = "展开";
                }
                mq_flog_01 = mq_flog_01 * -1;
            }
            this.style.background = "#f6f6f6";
            this.style.color = "#ff6633";
            arr_country[this.index].style.display = "block";
        }
    }

    /*品牌的收起和展开  和 选中改变颜色*/
    var updownFlog02 = -1;
    $('#unfold_02').click(function () {
        if (updownFlog02 == -1) {
            $(this).text('收起');
            $(this).css('background', 'url(images/list/down.png) no-repeat right');
            $('#all_brand').css('display', 'block');

        } else {
            $(this).text('展开');
            $(this).css('background', 'url(images/list/up.png) no-repeat right')
            $('#all_brand').css('display', 'none');

        }
        updownFlog02 = updownFlog02 * -1;
    });


    $('#brand_center span').click(function () {
        $(this).toggleClass('span-active');
        var is_cf = isrepetition($(this).text());
        var is_kong = is_temp();
        if (is_kong) {

        } else {
            $('#all_selected_del').text("清空已选");
        }
        if (is_cf) {
            $('#condition_selected_del').append('<span>' + $(this).text() + '</span>');
            shanchu();
        } else {
            alert("'" + $(this).text() + "'" + '已经被添加过了！');
        }
    });

    /*取件目的地  收起和展开  和 选中改变颜色*/
    var updownFlog03 = -1;
    $('#unfold_03').click(function () {
        if (updownFlog03 == -1) {
            $(this).text('收起');
            $(this).css('background', 'url(images/list/down.png) no-repeat right');
            $('#all_distination').css('display', 'block');

        } else {
            $(this).text('展开');
            $(this).css('background', 'url(images/list/up.png) no-repeat right')
            $('#all_distination').css('display', 'none');
        }
        updownFlog03 = updownFlog03 * -1;
    });

    $('#distination_center span').click(function () {

        $(this).toggleClass('span-active');

        var is_cf = isrepetition($(this).text());
        var is_kong = is_temp();
        if (is_kong) {
        } else {
            $('#all_selected_del').text("清空已选");
        }
        if (is_cf) {
            $('#condition_selected_del').append('<span>' + $(this).text() + '</span>');
            shanchu();
        } else {
            alert("'" + $(this).text() + "'" + '已经被添加过了！');
            //TODO 添加同名选项时颜色表示有误
            $(this).removeClass('span-active');
        }

    });


    /*取件方式改变颜色*/
    $('#way_right span').click(function () {

        $(this).toggleClass('span-active');
        var is_cf = isrepetition($(this).text());
        if (is_cf) {
            $('#condition_selected_del').append('<span>' + $(this).text() + '</span>');
            shanchu();
        } else {
            alert("'" + $(this).text() + "'" + '已经被添加过了！');
        }

    });

    /*取件时间改变颜色*/

    $('#pickTime_right span').click(function () {

        $(this).toggleClass('span-active');
        var is_cf = isrepetition($(this).text());
        if (is_cf) {
            $('#condition_selected_del').append('<span>' + $(this).text() + '</span>');
            shanchu();
        } else {
            alert("'" + $(this).text() + "'" + '已经被添加过了！');
        }

    });


    /*已选条件的删除  函数*/
    function shanchu() {
        $('#condition_selected_del span').click(function () {
            if ($('#condition_selected_del span').length == 1) {
                $('#all_selected_del').text("暂无选择");
            } else {
            }
            $(this).remove();

        });

        $('#all_selected_del').click(function () {
            $('#condition_selected_del span').remove();
            $('#all_selected_del').text("暂无选择");
        });

    }

    /*已选条件的删除*/

    $('#condition_selected_del span').click(function () {
        if ($('#condition_selected_del span').length == 1) {
            $('#all_selected_del').text("暂无选择");
        } else {
        }
        $(this).remove();

    });

    $('#all_selected_del').click(function () {
        $('#condition_selected_del span').remove();
        $('#all_selected_del').text("暂无选择");
    });
    /*重复判断*/


    function isrepetition(str) {


        var is_exit = 0;
        $('#condition_selected_del span').each(function () {
            //i为元素的索引，从0开始,
            //e为当前处理的元素
            if ($(this).text() == str) {
                is_exit++;
            } else {
            }

        });
        if (is_exit > 0) {
            return false;
        } else {
            return true;
        }

    }

    /*判断是否为空*/
    function is_temp() {
        if ($('#condition_selected_del span').length == 0) {
            return false;
        } else {
            return true;
        }
    }

    /*产品标题改变颜色*/
    $('#productTop_right span').click(function () {

        $('#productTop_right span').each(function () {
            $(this).removeClass('span-active');
        });

        $(this).toggleClass('span-active');
    });
});


// JavaScript Document

/*****************数据改变  ul 不变*************/
//根据数据写入 li
clipInit = function () {
    pageCon = 50;   //可更改
    liTab = 7;    //可更改
    medCur = Math.ceil(liTab / 2);
    var str = "";
    str += "<ul>";
    str += "<li class='disbled' id='firstPage' onclick='FirstPage()'>首页</li>";
    str += "<li class='disbled' id='lastPage' onclick='LastPage()'>上一页</li>";
    str += "<div id='pageU' class='fl'>";
    if (liTab <= pageCon) {
        for (var i = 1; i <= liTab; i++) {
            str += "<li id='clip" + i + "' onclick='pageInt(&#039;clip" + i + "&#039;,&#039;" + liTab + "&#039;,&#039;" + medCur + "&#039;)'>" + i + "</li>";
        }
    } else {
        for (var i = 1; i <= pageCon; i++) {
            str += "<li id='clip" + i + "' onclick='pageInt(&#039;clip" + i + "&#039;,&#039;" + pageCon + "&#039;,&#039;" + medCur + "&#039;)'>" + i + "</li>";
        }
    }
    str += "<li class='clear'></li>";
    str += "</div>";
    str += "<li class='BORDER' id='nextPage' onclick='NextPage()'>下一页</li>";
    str += "<li class='BORDER' id='endPage' onclick='EndPage()' style='border-right:1px solid #ccc'>尾页</li>";
    str += "<li class='clear'></li>";
    str += "</ul>";
    $("#clipDIV").html(str);
    pageInt('clip1', pageCon, medCur);
}
//设置当点击的值小于预设固定值
//单击事件  选择页数
clipPage = function (cur, page) {
    var str = "";
    for (var i = 1; i <= page; i++) {
        var liId = "clip" + i;
        if (cur == i) {
            $("#" + liId).attr("class", "curPage");
        } else {
            $("#" + liId).attr("class", "BORDER");
        }
        $("#" + liId).text(i);
    }
    pageControl(cur);
}
//设置的中转站，根据获取的值更改操作
pageInt = function (obj, page, medCur) {
    var value = parseInt($("#" + obj).text());
    if (value < medCur) {
        clipPage(value, page);
    } else if (value >= medCur) {
        clipPageMax(value, page, medCur);
    }
}
//设置当获取的值大于预设固定值
clipPageMax = function (cur, page, medCur) {
    var str = "";
    var startNum = cur - medCur + 1;
    var maxPage = startNum + parseInt(page) - 1;
    if (maxPage < pageCon) {
        for (var i = 1; i <= page; i++) {
            var liId = "clip" + i;
            if (medCur == i) {
                $("#" + liId).attr("class", "curPage");
            } else {
                $("#" + liId).attr("class", "BORDER");
            }
            $("#clip" + i).text(startNum);
            startNum++;
        }
    } else {
        var end = new RegExp(/\d+$/);
        var page = parseInt(end.exec(page));
        var curT = cur - pageCon + page;
        var maxP = pageCon;
        for (var i = page; i >= 1; i--) {
            var liId = "clip" + i;
            if (curT == i) {
                $("#" + liId).attr("class", "curPage");
            } else {
                $("#" + liId).attr("class", "BORDER");
            }
            $("#" + liId).text(maxP);
            maxP--;
        }

    }
    pageControl(cur);
}
//首页，尾页，上一页，下一页 的样式
pageControl = function (cur) {
    if (cur == 1) {
        $("#firstPage").attr("class", "disbled");
        $("#lastPage").attr("class", "disbled");
        $("#nextPage").attr("class", "BORDER");
        $("#endPage").attr("class", "BORDER");
    } else if (cur == pageCon) {
        $("#firstPage").attr("class", "BORDER");
        $("#lastPage").attr("class", "BORDER");
        $("#nextPage").attr("class", "disbled");
        $("#endPage").attr("class", "disbled");
    } else {
        $("#firstPage").attr("class", "BORDER");
        $("#lastPage").attr("class", "BORDER");
        $("#nextPage").attr("class", "BORDER");
        $("#endPage").attr("class", "BORDER");
    }
}
//第一页 显示
FirstPage = function () {
    var forNum = parseInt(liTab);
    clipPage(1, forNum);
}
//尾页 显示
EndPage = function () {
    var maxV = parseInt(pageCon);
    clipPageMax(maxV, liTab, medCur);
}
//上一页 显示
LastPage = function () {
    var choice = $(".curPage").attr('id');
    var obj = $("#" + choice).prev().attr('id');
    pageInt(obj, liTab, medCur);
}
//下一页 显示
NextPage = function () {
    var choice = $(".curPage").attr('id');
    var obj = $("#" + choice).next().attr('id');
    pageInt(obj, liTab, medCur);
}






