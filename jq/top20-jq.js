// add by liushang 2017/7/6
$(function () {

    /* 轮播图*/
    !function() {
        //获取图片的下标长度
        $(".img-box").width($(".img-box img").length * 1200);
        //初始化
        var n = 0;
        function run() {
            if (n < $(".img-box img").length - 1) {
                n += 1;
            } else {
                n = 1;
                $(".img-box").css({
                    marginLeft: 0
                });
            }
            $(".img-box").animate({
                    marginLeft: -1200 * n
                },
                500);
            $(".btn li").removeClass("Red");
            if (n == $(".img-box img").length - 1) {
                $(".btn li").eq(0).addClass("Red");
            } else {
                $(".btn li").eq(n).addClass("Red");
            }
        }
        //设置定时器
        var Timer = setInterval(run, 4000);
        $(".btn li").hover(function() {
                clearInterval(Timer);
                //获取图片的位置
                n = $(".btn li").index(this);
                $(".img-box").animate({
                        marginLeft: -1200 * n
                    },
                    500);
                $(".btn li").removeClass("Red").eq(n).addClass("Red");
            },
            function() {
                Timer = setInterval(run, 5000);
            });
    } ();

    !function() {
            $(".container img .container dd").hide().first().show();
            //$(".container img,.container dd").hide();
            //$(".container img:eq(0),.container dd:eq(0)").show();
            var n = 0;
            function run() {
                if (n < $(".container img").length - 1) {
                    n += 1
                } else {
                    n = 0;
                }
                $(".container img,.container dd").hide();
                $(".container img:eq(" + n + "),.container dd:eq(" + n + ")").show();
            }
            var setTimer = setInterval(run, 5000);
            //鼠标经过控制轮换图的动态
            $(".container").hover(function() {
                    clearInterval(setTimer);
                },
                function() {
                    setTimer = setInterval(run, 5000);
                });

        } ();
            /*产品标题改变颜色*/
    $('#productTop_right span').click(function(){

        $('#productTop_right span').each( function(){
            $(this).removeClass('span-active');
        });

        $(this).toggleClass('span-active');
    });



    var ban_img_mun = $('#img-box img').length;
    for(var n= 1;n < ban_img_mun; n++){
        if( n == 1){
            $('#banner_btn_num').append('<li class="Red"></li>');
        }else{
            $('#banner_btn_num').append('<li></li>');
        }

    }
    (function scaleThePic(){
        $('.section4-pro-lis').hover(function () {
            $(this).find('.section4-pro-lis-imgs').css('transform','scale(1.1)');
            },function () {
                $(this).find('.section4-pro-lis-imgs').css('transform','scale(1)');
            });

            $('.section5-pro-right-line-pro').hover(function () {
                $(this).find('.section5-pro-right-line-pros-pic-imgs').css('transform','scale(1.1)');
            },function () {
                $(this).find('.section5-pro-right-line-pros-pic-imgs').css('transform','scale(1)');
            });

            $('.section6-pro-line1-left').hover(function () {
                $(this).find('.section6-pro-line1-left-pic-imgs').css('transform','scale(1.2)');
            },function () {
                $(this).find('.section6-pro-line1-left-pic-imgs').css('transform','scale(1)');
        });
    }());
});


    