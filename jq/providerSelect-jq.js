/*add by liushang 2017/7/10*/
//记录当前的二级类目、三级类目
var category1; //二级分类
var category2; //三级分类
$(document).ready(function() {
    var cateList = [{
        "name": "一级分类1",
        "cate2": [{
            "name": "二级分类1_1",
            "cate3": [{
                "name": "三级分类1_1_1"
            }, {
                "name": "三级分类1_1_2"
            }]
        }, {
            "name": "二级分类1_2",
            "cate3": [{
                "name": "三级分类1_2_1"
            }, {
                "name": "三级分类1_2_2"
            }]
        }]
    }, {
        "name": "一级分类2",
        "cate2": [{
            "name": "二级分类2_1",
            "cate3": [{
                "name": "三级分类2_1_1"
            }, {
                "name": "三级分类2_1_2"
            }]
        }, {
            "name": "二级分类2_2",
            "cate3": [{
                "name": "三级分类2_2_1"
            }, {
                "name": "三级分类2_2_2"
            }]
        }]
    }];



    //从分类列表中读取 一级类目 ，并添加到DOM中
    for (var i = 0; i < cateList.length; i++) {
        var text0 = cateList[i].name;
        var cate0Name = '<li>' + text0 + '</li>';
        $('.section2-content-1 ul').append(cate0Name);
    }

    var selectList1 = $('.section2-content-1 ul li');
    // 选 择 一 级 类 目
    selectList1.click(function() {
        var $this = $(this);
        var result1 = $this.text();
        $('.section2-content-2 ul').empty(); //新添加前先清除掉原来的二级类目
        $('.section2-content-3 ul').empty(); //新添加前先清除掉原来的三级类目
        //判断选择当前选择的一级类目下的目录
        for (var i = 0; i < cateList.length; i++) {
            if (cateList[i].name === result1) {
                category1 = cateList[i].cate2;
            }
        }
        //从选择的一级类目选项中的所有二级类目，并添加到DOM中
        for (var j = 0; j < category1.length; j++) {
            var text1 = category1[j].name;
            var cate1Name = '<li onclick="select2(' + j + ')"><input type="checkbox"><label>' + text1 + '</label></li>';
            $('.section2-content-2 ul').append(cate1Name);
        }
        //先判断是否被选中过，若首次选中，则将选中的类目添加到下面的“已选类目”中
        if (!$this.hasClass('checked')) {
            $this.addClass('checked'); // 点击后添加选中状态
            var appendText = '<div class="content-selected"><div class="selected-1"><div>' +
                result1 +
                '</div></div><div class="selected-2"></div><div class="selected-3"></div></div>';
            $('.section2-content-4').append(appendText);
        } else {
            // TODO`
        }
    });

    function appendStr() {
        var appendStr = '<img src="./images/provider/dele.png">';
        $('.selected-1 > div,.selected-2 > div,.selected-3 > div').append(appendStr);
    }


});
//添加二级类目
function select2(n) {
    var $this = $('.section2-content-2 ul li').find('label').eq(n);
    var result2 = $this.text();
    $('.section2-content-3 ul').empty(); //新添加前先清除掉原来的三级类目
    //判断选择当前选择的二级类目下的目录
    for (var i = 0; i < category1.length; i++) {
        if (category1[i].name === result2) {
            category2 = category1[i].cate3;
        }
    }
    //从选择的二级类目选项中的所有三级类目，并添加到DOM中
    for (var j = 0; j < category2.length; j++) {
        var text2 = category2[j].name;
        var cate2Name = '<li onclick="select3(' + j + ')"><input type="checkbox"><label>' + text2 + '</label></li>';
        $('.section2-content-3 ul').append(cate2Name);
    }
    //先判断是否被选中过，若首次选中，则将选中的类目添加到下面的“已选类目”中
    if (!$this.hasClass('checked')) {
        $this.addClass('checked');
        var appendText = '<div>' + result2 + '</div>';
        $('.content-selected:last-child .selected-2').append(appendText);
    } else {
        // TODO
    }
}
//添加三级类目
function select3(n) {
    var $this = $('.section2-content-3 ul li').find('label').eq(n);
    var result3 = $this.text();
    //先判断是否被选中过，若首次选中，则将选中的类目添加到下面的“已选类目”中
    if (!$this.hasClass('checked')) {
        $this.addClass('checked');
        var appendText = '<div>' + result3 + '</div>';
        $('.content-selected:last-child .selected-3').append(appendText);
    } else {

    }
}